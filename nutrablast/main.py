#!/usr/bin/env python
import optparse
from nutrablast import Ingredients, Ingredient

def main():
	parser = optparse.OptionParser(
				description="Builds recipes for nutrabullet extractor", 
				version="0.1.0")

	""" Options and arguments """
	parser.add_option('--list', action="store_true", help="Lists ingredients loaded")

	v = optparse.Values()
	options, arguments = parser.parse_args(values=v)

	i = Ingredients()
	i.load('[{"name": "Carrot", "complimentary":[], "uncomplimentary":[]}]')

	""" List Ingredients if option is set """
	if hasattr(options, 'list'):
		i.print_list()

""" Not sure what this does? """
if __name__ == '__main__':
	main()