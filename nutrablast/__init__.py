#!/usr/bin/env python
import json

""" Recipe class
	
	Builds up a list of ingredients with help from the ingredients class
	Some rules for this class:
		- There must be an element of randomness in ingredients
		- No Ingredient must be paired with another that is uncomplimentary to it
		- Recipe must consist of atleast 3 ingredients
"""
class Recipe:

	def __init__(self):
		self.recipe = []

""" Ingredients class
	
	Stores and provides access to ingredients used in the recipes,
	Some rules prior to writing this class:
		- Should include ingredient name and other ingredients it may or may not go well with.
		- Should be able to list all ingredients
		- Should be able to add ingredients from a json list
		- Should be able to add single ingredients as well
"""
class Ingredients:

	def __init__(self):
		self.list = []

	""" 
		Load an ingredient list via a json string 
	"""
	def load(self, str):
		ingredients = json.loads(str)

		for i in ingredients:	
			ingredient = Ingredient(i.get('name'), i.get('complimentary'), i.get('uncomplimentary'))
			self.list.append(ingredient)

	def print_list(self):
		""" Print ingredients, in a nice way """
		for item in self.list:
			print item.name



""" Ingredient Object class
	An Object that contains a single ingredient information
"""
class Ingredient(object):

	def __init__(self, name, complimentary, uncomplimentary):
		self.name = name
		self.complimentary = complimentary
		self.uncomplimentary = uncomplimentary