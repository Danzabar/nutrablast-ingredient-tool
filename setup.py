from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='nutrablast-recipe',
    version='0.1.0',
    description='Gives a recipe to use with a Nutrabullet extractor',
    long_description=long_description,
    url='',
    author='Daniel Cox',
    author_email='danzabian@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Operating System :: POSIX :: BSD',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Utilities',
    ],
    keywords='nutrabullet recipe nutrabullet',
    packages=['nutrablast'],
    install_requires=[],
    tests_require=[
        'py>=1.4.20',
        'pytest>=2.5.2'
    ],
     entry_points={
        'console_scripts': [
            'nutrablast-recipe = nutrablast.main:main',
        ],
    },
)